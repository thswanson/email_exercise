package exercise24;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** The EmailAddress class represents an email address. Here only a
very limited variant is used that allows an address to be defined
and verified. A typical usage example is:
EmailAddress ea = new EmailAddress("john@company.com");
boolean isProper = ea.isValid();

Please note that the implementation of isValid is on purpose WRONG!

 This source code is from the book 
   "Flexible, Reliable Software:
     Using Patterns and Agile Development"
   published 2010 by CRC Press.
 Author: 
   Henrik B Christensen 
   Computer Science Department
   Aarhus University
 
 This source code is provided WITHOUT ANY WARRANTY either 
 expressed or implied. You may study, use, modify, and 
 distribute it for non-commercial purposes. For any 
 commercial use, see http://www.baerbak.com/
*/
public class EmailAddress {
private String address;
public EmailAddress(String address) {
  this.address = address;
}
/**
 * Verify that a string is an email address.
 * The string must obey the following (somewhat strict format)
 * <email>      ::= <identifier> @ <identifier> {. <identifier>}
 * <identifier> ::= letter { letter | digit }
 * 
 * Example: abc@somewhere12.mail.com is correct whereas the
 * following are incorrect: 
 *
 * 13a@s.m.com (identifier starting with digit)
 * a-c@s.m.com (non letter in identifier)
 * a.b.c (missing @)
 * abc@ (missing identifier after @)
 * @return true if the address obeys this format.
*/
//Adam Czeranko helped me a bit with the regex because I suck at them 
public boolean isValid() {
  // This implementation is wrong and could be expressed more
  // elegantly using regular expressions.
  Pattern p = Pattern.compile("[a-zA-Z]\\w*\\@[a-zA-Z]\\w*(\\.[a-zA-Z]\\w*)*");
  Matcher m = p.matcher(address);
  /*if ( address == null ) return false;
  if ( address.length() == 0 ) return false;
  int atPosition = address.indexOf( '@' );
  if ( atPosition == -1 ) return false;
  int dotPosition = address.indexOf('.');
  if ( dotPosition < atPosition ) return false;*/
  return m.matches();
}
public String get_address (){
	return address;
}
public static void main(String [] args) {
	EmailAddress e1 = new EmailAddress("t12@abc.def.gb");
	EmailAddress e2 = new EmailAddress("john@cs.edu");
	EmailAddress e3 = new EmailAddress("123@cs.edu");
	EmailAddress e4 = new EmailAddress("john.cs.edu");
	EmailAddress e5 = new EmailAddress("john@cs-edu");
	System.out.println("Email 1 is valid: "+ e1.isValid());
	System.out.println("Email 2 is valid: "+ e2.isValid());
	System.out.println("Email 3 is valid: " + e3.isValid());
	System.out.println("Email 4 is valid: " + e4.isValid());
	System.out.println("Email 5 is valid: " + e5.isValid());
}
}
