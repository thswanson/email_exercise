package exercise24;

import org.junit.*;
import static org.junit.Assert.*;
import java.lang.String;

/** Test case class for testing emails address verification.

   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Computer Science Department
     Aarhus University
   
   This source code is provided WITHOUT ANY WARRANTY either 
   expressed or implied. You may study, use, modify, and 
   distribute it for non-commercial purposes. For any 
   commercial use, see http://www.baerbak.com/
*/
public class TestEmail {

  private EmailAddress address;
  
  @Test
  public void testIsValid1() {
	  EmailAddress e1 = new EmailAddress("t12@abc.def.gb");		
	  assertTrue(e1.isValid());	  
  }
  @Test
  public void testIsValid2() {
	  EmailAddress e2 = new EmailAddress("john@cs.edu");
	  assertTrue(e2.isValid());
  }
  @Test
  public void testIsValid3() {
	  EmailAddress e3 = new EmailAddress("123@cs.edu");
	  assertFalse(e3.isValid());
  }
  @Test
  public void testIsValid4() {
	  EmailAddress e4 = new EmailAddress("john.cs.edu");
	  assertFalse(e4.isValid());
  }
  @Test
  public void testIsValid5() {
	  EmailAddress e5 = new EmailAddress("john@cs-edu");
	  assertFalse(e5.isValid());
  }

  @Test
  public void replaceMe() {
    assertTrue(true);
  }
  

  @Test
  public void shouldAcceptJohnAtCsDotEdu() {
    EmailAddress ea = new EmailAddress("john@cs.edu");
    assertTrue( ea.isValid() );
  }
  @Test
  public void shouldNotAccept123AtCsDotEdu() {
    EmailAddress ea = new EmailAddress("123@cs.edu");
    assertFalse( ea.isValid() );
  }
  @Test
  public void shouldNotBeNull() {
	  EmailAddress ea = new EmailAddress("");
	  assertNotNull(ea);
  }
  @Test
  public void shouldBeGreaterThanOne() {
	  EmailAddress no_length = new EmailAddress("");
	  assertFalse(no_length.get_address().length() > 0);
  }
  @Test
  public void positionOfAtBad() {
	  
	  EmailAddress this_should_fail = new EmailAddress("@youat.com");
	  
	  int isANeg = this_should_fail.get_address().indexOf('@');
	  
	  assertFalse(isANeg > 0);
  }
  @Test
  public void positionOfAtGood() {
	  EmailAddress where_is_the_at = new EmailAddress("tyler.swanson@hope.edu");
	  int notAtNeg = where_is_the_at.get_address().indexOf('@');
	  assertTrue(notAtNeg > 0);
  }
  
  @Test
  public void dotPositionGood() {
	  EmailAddress good_dot_pos = new EmailAddress("thisisemail@email.com");
	  int at = good_dot_pos.get_address().indexOf('@');
	  int dot = good_dot_pos.get_address().indexOf('.');
	  assertTrue(dot > at);
  }
  @Test
  public void dotPositionBad() {
	  EmailAddress bad_dot_pos = new EmailAddress(".bademail@email.com");
	  int at = bad_dot_pos.get_address().indexOf('@');
	  int dot = bad_dot_pos.get_address().indexOf('.');
	  assertFalse(dot > at);
  }
  
}

