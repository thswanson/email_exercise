# README #
Tyler Swanson
2020/08/21
CSCI 321

### What is this repository for? ###

This repository is for the first assignment in 321. It involves writing jUnit tests to verify that an email is valid and then writing regex to check for a valid email address in a class.

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

I would recommend just cloning this into your eclipse work space somehow. Honestly I haven't done a ton of stuff with jUnit before so I don't really understand how to put it into a build path. 

### Contribution guidelines ###

This class has deprecated, do not contribute any further.
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Tyler Swanson is the owner.
* Repo owner or admin
* Other community or team contact